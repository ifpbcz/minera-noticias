function CriarListaHome(dados){

    var item_lista = '<div class="card-home">';
        item_lista += '<h3 class="title">'+ dados[0] +'</h3>';
        item_lista += '<div class="text">'+ dados[1] +'</div>';
        item_lista += '<h5>'+ dados[2]+'</h5>';
        item_lista += '</div>'; 
     
    return item_lista;
   }

   function CarregarXML(){

     urlxml = 'https://raw.githubusercontent.com/AmandaSImoes1707/RSS/master/RSS2.xml';

     $.get(urlxml, function(dados){
       AnalisaXML(dados);
     });
   }

   function AnalisaXML(xml){

     dadosLista = "";

     xmlDoc = $.parseXML(xml);
     $xml = $(xmlDoc);
     $filmes = $xml.find("item");
       
     $filmes.each(function(){
       titulo = $(this).find('title').text();
       descricao = $(this).find('description').text();
       datapub = $(this).find('pubDate').text();

         dadosLista += CriarListaHome([titulo, descricao,datapub]);
       });
       document.getElementById("home").innerHTML = dadosLista;
   }